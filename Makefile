#####################################################
#
#  Name: Makefile
#  Project: alpp
#  Author: Adrian Ciesielski
#  Creation Date: 2012-08-31
#  Copyright: (c) 2012 by Adrian Ciesielski
#  License: GNU LGPL v3 (see COPYING)
#  Version: 0.0.1
#
#####################################################

# Configuration

# uC parameters
DEVICE	= atmega88pa
F_CPU	= 8000000


# paths definitions
#AVR_INCLUDE	= /usr/local/avr/include/
#AVR_LIBS	= /usr/local/avr/lib


# internal configuration - DO NOT modify anything beyond this point
SRCPREFIX = src
OBJS	  = obj
RELEASEDIR   = ../alpp_release

OBJECTNAMES = utils/simulSerial hal/preg8 hal/preg16 hal/pgpopin hal/pgpipin hal/pusartcommon sys/pled
HDRSTOCOPYR = hal/gpio hal/pgpipin hal/pgpopin hal/preg16 hal/preg8 hal/pregcommon hal/ptypes hal/pusartcommon hal/pusart sys/pled utils/pcircbuffer utils/simulSerial

# compile flags
CFLAGS	 = -Iinclude -DDEBUG_LEVEL=0 --std=c++11 -I$(AVR_INCLUDE) -Os
CXXFLAGS = -Iinclude -DDEBUG_LEVEL=0 -I$(AVR_INCLUDE) -g

# list of object to build

PREOBJS	 = $(addsuffix .o, $(OBJECTNAMES) )
OBJECTS	 = $(addprefix $(SRCPREFIX)/, $(PREOBJS) )


HDRSPRERELASE = $(addsprefix include, $(HDRSTOCOPYR))
HDRSRELEASE = $(addsuffix .h, $(HDRSPRERELASE))

# command to compile .c objects
COMPILEC = avr-gcc -c -Wall -Os -DF_CPU=$(F_CPU) $(CFLAGS) -mmcu=$(DEVICE)
# command to compile .cpp objects
COMPILE	 = avr-g++ -c -Wall -Os -DF_CPU=$(F_CPU) $(CXXFLAGS) -mmcu=$(DEVICE)

help:
	@echo "Jak używać:"
	@echo "make lib ............ aby zbudować bibliotekę"
	@echo "make clean_lib ...... aby wyczyścić  skompilowane obiekty"
	@echo "make release ........ aby wyeksportować zbudowane obiekty"
	@echo "make clean_release .. aby usunąć wyeksportowane obiekty"
	@echo "make doc ............ aby zbudować dokumentację"
	@echo "make clean_doc ...... aby usunąć zbudowaną dokumentację"

doc: libalpp.a
	@doxygen
	
clean_lib:
	rm -fR *.o *.a $(OBJECTS)
	#find -iname *~ -delete

clean_release:
	rm -fR $(RELEASEDIR)/

doc_clean:
	rm -fR doc/*



lib: libalpp.a
	avr-size $< -t

#-C --mcu=$(DEVICE)



release: libalpp.a | $(RELEASEDIR)
	@cp -v libalpp.a $(RELEASEDIR)/
	@cp -Rv include/$(HDRSRELEASE) $(RELEASEDIR)

$(RELEASEDIR):
	mkdir $(RELEASEDIR)
dirs:
	@if [ ! -d obj ]; then mkdir obj; fi;

%.o: %.cpp
	$(COMPILE) $< -o $@

.o.c:
	$(COMPILEC) $< -o $@

.S.o:
	$(COMPILE) -x assembler-with-cpp -c $< -o $@

.c.s:
	$(COMPILE) -S $< -o $@



libalpp.a: $(OBJECTS)
	avr-ar rsc $@ $^




