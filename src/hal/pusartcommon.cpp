#include "hal/pusartcommon.h"

PUsartCommon::PUsartCommon(
     preg8_t &pUCSRA
    ,preg8_t &pUCSRB
    ,preg8_t &pUCSRC
    ,preg8_t &pUDR
    ,preg8_t &pUBRRL
    ,preg8_t &pUBRRH):
     ucsra(pUCSRA)
    ,ucsrb(pUCSRB)
    ,ucsrc(pUCSRC)
    ,ubrrl(pUBRRL)
    ,ubrrh(pUBRRH)
    ,udr(pUDR)
{
    //ctor
    ;
}

void PUsartCommon::config(uint16_t baudRate
            ,uint8_t dblSpeed   // UCSRA
            ,uint8_t clkPol     // UCSRC
            ,uint8_t mode       // UCSRC
            ,uint8_t stopbits   // UCSRC
            ,uint8_t parity     // UCSRC
            ,uint8_t charsize   // UCSRB/UCSRC
            )
{

    uint8_t baudr = baud2Reg(baudRate,1+dblSpeed);
    ubrrh = (uint8_t)(baudr>>8);
    ubrrl = (uint8_t)(baudr);

    ucsra = (dblSpeed<<P_U2Xn);
    uint8_t ninebits = 0b1 & (charsize>>2);
    ucsrb = (ninebits<< P_UCSZn2);

    ucsrc = (mode<<P_UMSELn0) | (parity<<P_UPMn0) | (stopbits<<P_USBSn) | ((0b11 & charsize)<<P_UCSZn0) | (clkPol<<P_UCPOLn);

    ucsrb |= _BV(P_RXENn) | _BV(P_TXENn);

}

uint8_t PUsartCommon::baud2Reg(uint16_t baudRate, uint8_t speed)
{
        return (((F_CPU/100)*speed)/(16*baudRate)-1);
}

void PUsartCommon::txChar(unsigned char Znak)
{
#ifdef BLOCKING_ACCESS
    while(!txReady());
    udr = Znak;
#else
    udr=Znak;
#endif // BLOCKING_ACCESS
}

unsigned char PUsartCommon::rxChar()
{
#ifdef BLOCKING_ACCESS
    while(!rxReady());
    return udr;
#else
    return udr;
#endif // BLOCKING_ACCESS
}
