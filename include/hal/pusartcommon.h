/*---------------------------------------------+
|                                              |
| Name: include/hal/pusart.h            |
|  Project: alpp                               |
|  Author: Adrian Ciesielski                   |
|  Creation Date: 2021-10-02                   |
|  Copyright: (c) 2021 by Adrian Ciesielski    |
|  License: See top of Makefile                |
|  Version: See top of Makefile                |
|                                              |
+---------------------------------------------*/


#ifndef PUSARTCOMMON_H
#define PUSARTCOMMON_H

#include "pusart.h"
#include "ptypes.h"

#define BLOCKING_ACCESS

class PUsartCommon //: public PUsart
{
	public:
        /**
         *
         *
         */
		PUsartCommon(preg8_t &pUCSRA,preg8_t &pUCSRB, preg8_t &pUCSRC, preg8_t &pUDR, preg8_t &pUBRRL, preg8_t &pUBRRH);

		/**
		 *
		 *
		 */
		void config(uint16_t baudRate
            ,uint8_t dblSpeed   = P_SPD_NORMAL  // UCSRA
            ,uint8_t clkPol     = P_XCK_FALLING // UCSRC
            ,uint8_t mode       = P_MODE_ASYNC  // UCSRC
            ,uint8_t stopbits   = P_STOPBITS_1  // UCSRC
            ,uint8_t parity     = P_PARITY_NONE // UCSRC
            ,uint8_t charsize   = P_CHARSIZE_8  // UCSRB/UCSRC
            );

        /**
		 *
		 *
		 */
        void setBaudRate(uint16_t baud);

        // inline void setClockPolarity(bool polarity);

		void txChar(unsigned char Znak);
		unsigned char rxChar();
		inline void rxInterruptEnable();
		inline void rxInterruptDisable();
		inline void txInterruptEnable();
		inline void txInterruptDisable();
        inline uint8_t txReady();
        inline uint8_t rxReady();
        inline uint8_t errcheck();

        enum BAUDRATE{
            P_BAUD_115200   = 1152,
            P_BAUD_57600    = 576,
            P_BAUD_56000    = 560,
            P_BAUD_38400    = 384,
            P_BAUD_28800    = 288,
            P_BAUD_19200    = 192,
            P_BAUD_14400    = 144,
            P_BAUD_9600     = 96,
            p_BAUD_4800     = 48
        };

        enum U_DBLSPD{
            P_SPD_NORMAL = 0b0,
            P_SPD_DOUBLE = 0b1
        };

        enum U_MODE{
            P_MODE_ASYNC       = 0b00,
            P_MODE_SYNC        = 0b01,
            P_MODE_SPI_MASTER  = 0b11
        };

        enum U_STOPBITS{
            P_STOPBITS_1 = 0,
            P_STOPBITS_2 = 1
        };

        enum U_PARITY{
            P_PARITY_NONE = 0b00,
            P_PARITY_EVEN = 0b10,
            P_PARITY_ODD  = 0b11
        };

        enum U_CHARSIZE{
            P_CHARSIZE_5  = 0b000,
            P_CHARSIZE_6  = 0b001,
            P_CHARSIZE_7  = 0b010,
            P_CHARSIZE_8  = 0b011,
            P_CHARSIZE_9  = 0b111
        };

        enum U_UCPOL{
            P_XCK_FALLING = 0,
            P_XCK_RISING  = 1
        };


        enum ERRORS{
            P_ERR_PARITY = (1<<2),
            P_ERR_DOVERRUN = (1<<3),
            P_ERR_FRAME = (1<<4),
        };



	protected:
        //default definition of abstract bit names inside the corresponding registers. Typically almost all AVR devices shares the same layout of bit positions. But there may be exceptions in any case
        enum UCSRA_BITS{
            P_MPCMn    = 0,
            P_U2Xn     = 1,
            P_UPEn     = 2,
            P_DORn     = 3,
            P_FEn      = 4,
            P_UDREn    = 5,
            P_TXCn     = 6,
            P_RXCn     = 7,
        };

        enum UCSRB_BITS{
            P_TXB8n    = 0,
            P_RXB8n    = 1,
            P_UCSZn2   = 2,
            P_TXENn    = 3,
            P_RXENn    = 4,
            P_UDRIEn   = 5,
            P_TXCIEn   = 6,
            P_RXCIEn   = 7,
        };

        enum UCSRC_BITS{
            P_UCPOLn   = 0,
            P_UCSZn0   = 1,
            P_UCSZn1   = 2,
            P_USBSn    = 3,
            P_UPMn0    = 4,
            P_UPMn1    = 5,
            P_UMSELn0  = 6,
            P_UMSELn1  = 7,
        };

	private:
        preg8_t &ucsra;  // holding UCSRnA register reference
        preg8_t &ucsrb;  // holding UCSRnB register reference
        preg8_t &ucsrc;  // holding UCSRnC register reference
        preg8_t &ubrrl;  // holding UBRRn register reference
        preg8_t &ubrrh;  // holding UBRRn register reference
        preg8_t &udr;    // holding UDRn register

        uint8_t baud2Reg(uint16_t baudRate, uint8_t speed=1);


};


inline uint8_t PUsartCommon::txReady()
{
    return (ucsra & (_BV(P_UDREn)));
}

inline uint8_t PUsartCommon::rxReady()
{
    return (ucsra & (_BV(P_RXCn)));
}

inline void PUsartCommon::rxInterruptEnable()
{
    ucsrb |= _BV(P_RXCIEn);
}

inline void PUsartCommon::rxInterruptDisable()
{
    ucsrb &= ~_BV(P_RXCIEn);
}

inline void PUsartCommon::txInterruptEnable()
{
    ucsrb |= _BV(P_TXCIEn);
}

inline void PUsartCommon::txInterruptDisable()
{
    ucsrb &= ~_BV(P_TXCIEn);
}

inline uint8_t PUsartCommon::errcheck()
{
    return ucsra & (P_ERR_FRAME | P_ERR_DOVERRUN | P_ERR_PARITY);
}

#if 0
inline void PUsartCommon::setClockPolarity(bool polarity)
{
    if(polarity)
    {
        ucsrc |=  _BV(P_UCPOLn);
        ucsra &= ~_BV(P_U2Xn);  // setting UCPOLn requests U2Xn to reset
    } else {
        ucsrc &= ~_BV(P_UCPOLn);
    }
}
#endif // 0

#endif // PUSARTCOMMON_H
