/*---------------------------------------------+
|                                              |
| Name: include/hal/pusart.h            |
|  Project: alpp                               |
|  Author: Adrian Ciesielski                   |
|  Creation Date: 2021-10-02                   |
|  Copyright: (c) 2021 by Adrian Ciesielski    |
|  License: See top of Makefile                |
|  Version: See top of Makefile                |
|                                              |
+---------------------------------------------*/



#ifndef PUSART_H
#define PUSART_H
#include <avr/io.h>

class PUsart
{
	public:
        /**
         * 
         * 
         */
		PUsart();
		virtual void txChar(unsigned char Znak);
		virtual unsigned char rxChar();
        virtual uint8_t txReady();
        virtual uint8_t rxReady();
        virtual uint8_t errcheck();
	protected:
        
	private:

};

#endif // PUSART_H
